use rand::prelude::*;
use random_string::generate;
use std::fs;
use rand::Rng;
use std::any::type_name;
/**
Hang man Guessing game, made by multiple students at UMD
for Hacktoberfest. Will create a random word, and prompt the user
to guess. Game ends when user guesses correctly or hits the guesslimit.
*/
fn main(){
    hangman();
}

fn hangman() {
    //using rand Rng
    let mut rng = rand::thread_rng();
    let mut validOption = 0;
    
    //Gonna leave this alone until I figure out input from console. Game will run as it has been running.
    let mut option = "".to_string();
    
    while validOption==0 {
        option = "".to_string();
        println!("CHOOSE WISELY (enter either 1 or 2):\n(1): Guess a random word.\n(2): Guess an actual word.");
        let thisOption = std::io::stdin().read_line(&mut option).unwrap();
        option = option.to_string();
        if option == "2\n".to_string() || option == "1\n".to_string(){
            validOption = 1;
        }
        else if option == ("2147483647\n".to_string()){
            println!("CAN WE GET MUCH HIGHER?\n");
        }
    }

    

    //variables that don't depend on the option
    let charset = "abcdefghijklmnopqrstuvwxyz";
    //mutables
    let mut wordlength:i32 = 0;
    let mut guesses:i32 = 0;
    let mut guesslimit:i32 = 0;
    let mut word = "".to_string();

    
    if option == "1\n".to_string() {
        println!("Option 1");
        wordlength = random();
        if wordlength < 0{
            wordlength *= -1;
        }
        guesslimit = rng.gen_range(wordlength..(wordlength*2));
       
        let createdword: Vec<char> = Vec::new();
        wordlength = (wordlength % 20) + 1;
        guesslimit = (guesslimit % 20) + 5;
        word = generate(wordlength as usize, charset);
    }

    else if option == "2\n".to_string(){
        let fileContents = fs::read_to_string("./corpus.txt").expect("Couldn't read.");
        //println!("{fileContents}");
        let wordVec:Vec<&str> = fileContents.split(" ").collect();
        
        let vecLength = wordVec.len();

        let wordIndex = rng.gen_range(0..(vecLength)-1);

        //let word = wordVec[wordIndex];
        word = wordVec[wordIndex].to_string();
        wordlength = word.len() as i32;
        guesslimit = rng.gen_range(wordlength..(wordlength*2));
    }

   
    
	let mut guess_list: Vec<String> = Vec::new();
    let mut line:String = "".to_string();
    println!("Welcome to Hangman-Rust\nThe wordlength is {}, guess limit is {}, and legal characters are {}", wordlength, guesslimit, charset);
    let gameloop:bool = true;


    //need vector of characters in word
    let mut word_char_list: Vec<char> = word.chars().collect();
    let mut word_char_string_list: Vec<String> = Vec::new();
    for thisC in word_char_list{
        word_char_string_list.push(thisC.to_string());
    }
    //memory safety in rust is crazy
    let mut word_char_string_list_clone = word_char_string_list.clone();
    word_char_string_list_clone.sort();
    word_char_string_list_clone.dedup();
    
    /*
    for c in word_char_string_list_clone.clone(){
        println!("{c}");
    }  
    */
    //Guess limit should be higher than word length, implement later.
    
    //for checking if winning works
    //println!("{word}");
    
    while gameloop {
    	println!("current guesses are: \n {:?}", guess_list); 
        if guesses > guesslimit{ 
            
            println!("Sorry you lose :(");
            break;
        }
    
        println!("You have already guessed this many times: {}", guesses);

        println!("Enter your guess");
        let guess = std::io::stdin().read_line(&mut line).unwrap();
		line = line.remove(0).to_string();
		guess_list.push(line);

        //WIN CONDITION: Guess list must have same characters as word
        let mut guess_list_clone = guess_list.clone();
        guess_list_clone.sort();
        guess_list_clone.dedup();
        if guess_list_clone == word_char_string_list_clone{
            println!("DING DING DING YOU WIN!!!!!");
            break;
        }
        

		//println!("this is line:{} this is guess:{}",line,guess);
	    guesses+=1;
        line = "".to_string();

    }
}
